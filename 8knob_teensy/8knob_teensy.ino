#include <SPI.h>

//#define DEBUG 1

const int chipSelectPin = 10;
const byte pins = 8;
const byte configBits = 0b00001100; // single
//  const byte configBits = 0b00001000; // differential

bool firstLoop = true;
int count = 0;
int oldValues[8];
int values[8];
bool changed[8];

byte msb[8];
byte lsb[8];

unsigned long startTime;
unsigned long time;

SPISettings settings(2000000,MSBFIRST,SPI_MODE0);

void setup() {
  Serial.begin(31250);
  
  Serial.println("I AM MIDI MAN");

  pinMode(chipSelectPin, OUTPUT);
  digitalWrite(chipSelectPin, HIGH);
  
  SPI.begin();

  startTime = millis();
}

int readPin(byte pin) {
  SPI.beginTransaction(settings);
  digitalWrite(chipSelectPin, LOW);
  
  SPI.transfer(configBits | (pin >> 1));
  int hi = SPI.transfer(pin << 7);
  int lo = SPI.transfer(0b00000000);

  digitalWrite(chipSelectPin, HIGH);
  SPI.endTransaction();
  
  int adcValue = ((hi & 0b00001111) << 8) + lo;

  //  byte sign = hi & 0b00010000;

  //  if (sign) {
  //    adcValue -= 4096;
  //  }

  // We get to invert the values because we wired the damn thing wrong just like on the joystick
  return 4095 - adcValue;
}

void logTime() {
  if (count == 1000) {
    count = 0;

    time = millis() - startTime;
    
    Serial.print("I did it! ");
    Serial.print(time);
    Serial.println();

    for (byte i = 0; i < pins; i++) {
      Serial.print(values[i]);
      Serial.print(" ");
    }

    Serial.println();

    startTime = millis();
  }
}

void loop() {
  // Ableton gets pissy if this isn't here for some reason
  usbMIDI.read();
  
  for (byte i = 0; i < pins; i++) {
    values[i] = readPin(i);

    // Check if it's changed by 10 to avoid noise being an issue
    changed[i] = (abs(oldValues[i] - values[i]) > 10);
    if (changed[i]) {
      oldValues[i] = values[i];

      if (firstLoop) {
        continue;
      }

      // If it's changed, we want to read it 16 times total to get 14-bit precision
      // We don't want to do this if it hasn't changed to avoid unnecessary reads
      unsigned long pinTotal = values[i];
      for (byte j = 0; j < 15; j++) {
        pinTotal += readPin(i);
      }

      values[i] = pinTotal / 4;
      
      msb[i] = (values[i] >> 7) & 127;
      lsb[i] = values[i] & 127;

      // We'll give up some accuracy to make sure we can always send min and max values
      if (msb[i] == 0 && lsb[i] < 64) {
        lsb[i] = 0;
      } else if (msb[i] == 127 && lsb[i] >= 64) {
        lsb[i] = 127;
      }

#ifdef DEBUG
      Serial.print("Pin ");
      Serial.print(i);
      Serial.print(": ");
      Serial.print(msb[i]);
      Serial.print(", ");
      Serial.print(lsb[i]);
      Serial.println();
      Serial.println(values[i]);
#else
      usbMIDI.sendControlChange(16 + i, msb[i], 1);
      usbMIDI.sendControlChange(48 + i, lsb[i], 1);
      usbMIDI.send_now();
#endif
    }
  }
  
  count++;

#ifdef DEBUG
//  logTime();
#endif

  firstLoop = false;
}
